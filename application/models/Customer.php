<?php
class Customer extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor a mysql
  function insertar($datos){
    return $this->db->insert("customer",$datos);
  }
  //funcion para consultar ingredientes
  public function obtenerTodos(){
    $listadoCustomers=$this->db->get("customer");
    if ($listadoCustomers->num_rows()>0) {
      return $listadoCustomers->result();
    }else{ //caundo no hay datos
      return false;
    }
  }
  //boorrar ingredientes
  function borrar($id_cus){
    $this->db->where("id_cus",$id_cus);
    if($this->db->delete("customer")) {
      return true;
    } else {
      return false;
    }
  }
  //funcion para consultar un ingrediente especifico
  function obtenerPorId($id_cus){
    $this->db->where("id_cus",$id_cus);
    $customer=$this->db->get("customer");
    if ($customer->num_rows()>0) {
      return $customer->row();
    }
      return false;
    }
    //funcion para actualizar un ingrediente
    function actualizar($id_cus,$datos){
      $this->db->where("id_cus",$id_cus);
      return $this->db->update("customer",$datos);
    }
}//cierre de clase

 ?>
