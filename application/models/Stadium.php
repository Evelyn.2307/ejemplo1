<?php
  class Stadium extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db
                ->insert("stadium",
                $datos);
    }
    //FUNCION PARA OBTENER TODOS LOS DATOS
    function obtenerTodos(){
      $listadoEstadios=$this->db->get("stadium");//nombre de la tabla en la BDD
      //para saber si hay datos o no hay datos
      if ($listadoEstadios->num_rows()>0) { //
        return $listadoEstadios->result();
      }else{
        return false;
      }

    }
    function borrar($id_stadium)
    {

      $this->db->where("id_stadium",$id_stadium);

      if ($this->db->delete("stadium")) {
        return true;
      } else {
        return false;
      }

    }

    //FUNCION PARA CONSULTAR UN PLANETA
    function obtenerPorId($id_stadium){
      $this->db->where("id_stadium", $id_stadium);
      $stadium=$this->db->get("stadium");
      if ($stadium->num_rows()>0) {
        return $stadium->row();
      } else {
         return false;
      }
    }
      //FUNCION PARA ACTUALIZAR UN PLANETA
      function actualizar($id_stadium,$data){
        $this->db->where("id_stadium",$id_stadium);
        return $this->db->update('stadium',$data);
      }
  }//Cierre de la clase

 ?>
