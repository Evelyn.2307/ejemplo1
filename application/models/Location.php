<?php
class Location extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor a mysql
  function insertar($datos){
    return $this->db->insert("location",$datos);
  }
  //funcion para consultar ingredientes
  public function obtenerTodos(){
    $listadoLocations=$this->db->get("location");
    if ($listadoLocations->num_rows()>0) {
      return $listadoLocations->result();
    }else{ //caundo no hay datos
      return false;
    }
  }
  //boorrar ingredientes
  function borrar($id_loc){
    $this->db->where("id_loc",$id_loc);
    if($this->db->delete("location")) {
      return true;
    } else {
      return false;
    }
  }
  //funcion para consultar un ingrediente especifico
  function obtenerPorId($id_loc){
    $this->db->where("id_loc",$id_loc);
    $location=$this->db->get("location");
    if ($location->num_rows()>0) {
      return $location->row();
    }
      return false;
    }
    //funcion para actualizar un ingrediente
    function actualizar($id_loc,$datos){
      $this->db->where("id_loc",$id_loc);
      return $this->db->update("location",$datos);
    }
}//cierre de clase

 ?>
