<?php
class Partido extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor a mysql
  function insertar($datos){
    return $this->db->insert("partido",$datos);
  }
  //funcion para consultar eventos
  public function obtenerTodos(){
    $listadoPartidos=$this->db->get("partido");
    if ($listadoPartidos->num_rows()>0) {
      return $listadoPartidos->result();
    }else{ //caundo no hay datos
      return false;
    }
  }
  //boorrar evento
  function borrar($id_partido){
    $this->db->where("id_partido",$id_partido);
    if($this->db->delete("partido")) {
      return true;
    } else {
      return false;
    }
  }
  //funcion para consultar un evento
  function obtenerPorId($id_partido){
    $this->db->where("id_partido",$id_partido);
    $partido=$this->db->get("partido");
    if ($partido->num_rows()>0) {
      return $partido->row();
    }
      return false;
    }
    //funcion para actualizar un evento
    function actualizar($id_partido,$datos){
      $this->db->where("id_partido",$id_partido);
      return $this->db->update("partido",$datos);
    }
}//cierre de clase

 ?>
