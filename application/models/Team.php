<?php
  class Team extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("team",$datos);
    }
    //FUNCION PARA OBTENER TODOS LOS DATOS
    public function obtenerTodos(){
      $listadoTeams=$this->db->get("team");//nombre de la tabla en la BDD
      //para saber si hay datos o no hay datos
      if ($listadoTeams->num_rows()>0) { //
        return $listadoTeams->result();
      }else{
        return false;
      }

    }
    function borrar($id_te){
      $this->db->where("id_te",$id_te);
      if ($this->db->delete("team")) {
        return true;
      } else {
        return false;
      }

    }

    //FUNCION PARA CONSULTAR UN PLANETA
    function obtenerPorId($id_te){
      $this->db->where("id_te", $id_te);
      $team=$this->db->get("team");
      if ($team->num_rows()>0) {
        return $team->row();
      } else {
         return false;
      }
    }
      //FUNCION PARA ACTUALIZAR UN PLANETA
      function procesarActualizacion($id_te,$datos){
        $this->db->where("id_te",$id_te);
        return $this->db->update('team',$datos);
      }
  }//Cierre de la clase

 ?>
