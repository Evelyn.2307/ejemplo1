<?php
class Game extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor a mysql para evento
  function insertar($datos){
    return $this->db->insert("game",$datos);
  }
  //funcion para consultar eventos
  public function obtenerTodos(){
    $listadoGames=$this->db->get("game");
    if ($listadoGames->num_rows()>0) {
      return $listadoGames->result();
    }else{ //cuando no hay datos
      return false;
    }
  }
  //boorrar evento
  function borrar($id_eve){
    $this->db->where("id_eve",$id_eve);
    if($this->db->delete("game")) {
      return true;
    } else {
      return false;
    }
  }
  //funcion para consultar un evento
  function obtenerPorId($id_eve){
    $this->db->where("id_eve",$id_eve);
    $game=$this->db->get("game");
    if ($game->num_rows()>0) {
      return $game->row();
    }
      return false;
    }
    //funcion para actualizar un evento
    function actualizar($id_eve,$datos){
      $this->db->where("id_eve",$id_eve);
      return $this->db->update("game",$datos);
    }
}//cierre de clase

 ?>
