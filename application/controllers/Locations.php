<?php
class Locations extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    //cargamos el modelo
    $this->load->model('Location');
  }
  //funcion para renderizar la vista
  public function index(){
    $data['locations']=$this->Location->ObtenerTodos();
    $this->load->view('header');
    $this->load->view('locations/index',$data);
    $this->load->view('footer');
  }
  //funcion para renderizar la vista NUEVO
  public function new()
  {
    $this->load->view('header');
    $this->load->view('locations/new');
    $this->load->view('footer');
  }
  //funcion para guardar los datos
  public function save(){
    $datosNuevoLocation=array(
      //convocamos al modelo
      "gate_loc "=>$this->input->post('gate_loc '),
      "row_loc"=>$this->input->post('row_loc'),
      "seat_loc"=>$this->input->post('seat_loc'),
      "price_loc"=>$this->input->post('price_loc')
    );
    if ($this->Location->insertar($datosNuevoLocation)) {
      $this->session->set_flashdata("confirmation","Agregado correctamente");
    } else {
      $this->session->set_flashdata("error","Error al guardar intente denuevo");
    }
    redirect('locations/index');
  }
  //funcion para eliminar INGREDIENTES
  public function delete($id_loc){

    if($this->Location->borrar($id_loc)) {
      $this->session->set_flashdata("confirmacion","Eliminado exitosamente");
    }else{
      $this->session->set_flashdata("error","Error al eliminar");
    }
    redirect('locations/index');
  }
  //funcion para rendrizar vista editar con  ingredientes
  public function edit($id_loc){
    $data["locationEditar"]=$this->Location->obtenerPorId($id_loc);
    $this->load->view('header');
    $this->load->view('locations/edit',$data);
    $this->load->view('footer');
  }
  //proceso ACTUALIZAR
  public function procesarActualizacion(){
      $datosEditados=array(
        "gate_loc"=>$this->input->post('gate_loc'),
        "row_loc"=>$this->input->post('row_loc'),
        "seat_loc"=>$this->input->post('seat_loc'),
        "price_loc"=>$this->input->post('price_loc')
      );
      $id_loc=$this->input->post("id_loc");
      if ($this->Location->actualizar($id_loc,$datosEditados)) {
        $this->session->set_flashdata("confirmation","localidad actualizado exitosamente");
      } else {
        $this->session->flashdata("confirmation","Error al eliminar intente de nuevo");
      }
      redirect("locations/index");
    }
}//cierre de clase no borrar




 ?>
