<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teams extends CI_Controller {
	//constructor
	function __construct()
  {
    parent::__construct();
		//cargar modelo
		$this->load->model('team');

	}

	public function index()
	{
		$data['teams']=$this->team->obtenerTodos();
		$this->load->view('header')	;
		$this->load->view('teams/index',$data);
		$this->load->view('footer')	;
	}
  public function create()
  {
    $this->load->view('header')	;
    $this->load->view('teams/create');
    $this->load->view('footer')	;
  }
	public function save()
	{
		$datosCreateTeam=array(
			"photo_te"=>$this->input->post('photo_te'),
			"name_te"=>$this->input->post('name_te'),
			"city_te"=>$this->input->post('city_te')
		);
		$this->load->library("upload");
				$new_name = "photo_te" . time() . "_" . rand(1, 5000);
				$config['file_name'] = $new_name . '_1';
				$config['upload_path']          = FCPATH . 'uploads/';
				$config['allowed_types']        = 'jpeg|jpg|png';
				$config['max_size']             = 1024*5; //5 MB
				$this->upload->initialize($config);

				if ($this->upload->do_upload("photo_te")) {
					$dataSubida = $this->upload->data();
					$datosCreateTeam["photo_te"] = $dataSubida['file_name'];
				}

	if ($this->team->insertar($datosCreateTeam)) {

    redirect('teams/index');

  }else {
    echo "<h1>INSERT ERROR </h1>";
  }

	}
//funcion para Eliminar
	public function borrar ($id_te)
	{
    if ($this->team->borrar($id_te)) { //invocando al modelo
    redirect('teams/index');
  } else {
    echo "ERROR WHEN DELETING:C";
  }
		redirect ('teams/index');

	}
//FUNCION REEDERIZAR VISTA
public function edit($id_te){
	$data ["TeamEdit"]=$this->team->obtenerPorId($id_te);
	$this->load->view('header')	;
	$this->load->view('teams/edit',$data);
	$this->load->view('footer')	;
}

//PROCESO DE ACTUALIZACION
public function procesarActualizacion(){
	$datosEditados=array(
		"photo_te"=>$this->input->post('photo_te'),
    "name_te"=>$this->input->post('name_te'),
    "city_te"=>$this->input->post('city_te')
  );

  $this->load->library("upload");
      $new_name = "photo_te" . time() . "_" . rand(1, 5000);
      $config['file_name'] = $new_name . '_1';
      $config['upload_path']          = FCPATH . 'uploads/';
      $config['allowed_types']        = 'jpeg|jpg|png';
      $config['max_size']             = 1024*5; //5 MB
      $this->upload->initialize($config);

      if ($this->upload->do_upload("photo_te")) {
        $dataSubida = $this->upload->data();
        $datosEditados["photo_te"] = $dataSubida['file_name'];
      }
			if ($this->team->insertar($datosCreateTeam)) {

			  redirect('team/index');

			}else {
			  echo "<h1>ERRORE AL EDITAR </h1>";
			}

}
} // cierre de la clase
