<?php
class Games extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    //cargamos el modelo
    $this->load->model('Game');
  }
  //funcion para renderizar la vista evento
  public function index(){
    $data['games']=$this->Game->ObtenerTodos();
    $this->load->view('header');
    $this->load->view('games/index',$data);
    $this->load->view('footer');
  }
  //funcion para renderizar la vista NUEVO
  public function niew()
  {
    $this->load->view('header');
    $this->load->view('games/new');
    $this->load->view('footer');
  }
  //funcion para guardar los datos
  public function guardar(){
    $datosNewGame=array(
      //convocamos al modelo
      "day_eve"=>$this->input->post('day_eve'),
      "date_eve"=>$this->input->post('date_eve'),
      "hour_eve"=>$this->input->post('hour_eve')
    );
    if ($this->Game->insertar($datosNewGame)) {
      $this->session->set_flashdata("confirmacion","Agregado correctamente");
    } else {
      $this->session->set_flashdata("error","Error saving try again");
    }
    redirect('games/index');
  }
  //funcion para eliminar eventos
  public function eliminar($id_eve){

    if($this->Game->borrar($id_eve)) {
      $this->session->set_flashdata("confirmation","Deleted successfully");
    }else{
      $this->session->set_flashdata("error","Error deleting");
    }
    redirect('games/index');
  }
  //funcion para rendrizar vista editar con  eventos
  public function edit($id_eve){
    $data["gameEdit"]=$this->Game->obtenerPorId($id_eve);
    $this->load->view('header');
    $this->load->view('games/edit',$data);
    $this->load->view('footer');
  }
  //proceso ACTUALIZAR evento
  public function procesarActualizacion(){
      $datosEditados=array(
        "day_eve"=>$this->input->post('day_eve'),
        "date_eve"=>$this->input->post('date_eve'),
        "hour_eve"=>$this->input->post('hour_eve')
      );
      $id_eve=$this->input->post("id_eve");
      if ($this->Game->actualizar($id_eve,$datosEditados)) {
        $this->session->set_flashdata("confirmation","game updated successfully");
      } else {
        $this->session->flashdata("confirmation","Error deleting try again");
      }
      redirect("games/index");
    }
}//cierre de clase no borrar


 ?>
