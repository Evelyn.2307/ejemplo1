<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stadiums extends CI_Controller {
	//constructor
	function __construct()
  {
    parent::__construct();
		//cargar modelo
		$this->load->model('stadium');

	}

	public function index()
	{
		$data['stadiums']=$this->stadium->obtenerTodos();
		$this->load->view('header')	;
		$this->load->view('stadiums/index',$data);
		$this->load->view('footer')	;
	}
  public function new()
  {
    $this->load->view('header')	;
    $this->load->view('stadiums/new');
    $this->load->view('footer')	;
  }
	public function guardar()
	{
		$datosNuevoEstadio=array(
			"name_stadium"=>$this->input->post('name_stadium'),
			"address_stadium"=>$this->input->post('address_stadium')
		);

		$this->load->library("upload");
				$new_name = "photo_stadium" . time() . "_" . rand(1, 5000);
				$config['file_name'] = $new_name . '_1';
				$config['upload_path']          = FCPATH . 'uploads/';
				$config['allowed_types']        = 'jpeg|jpg|png';
				$config['max_size']             = 1024*5; //5 MB
				$this->upload->initialize($config);

				if ($this->upload->do_upload("photo_stadium")) {
					$dataSubida = $this->upload->data();
					$datosNuevoEstadio["photo_stadium"] = $dataSubida['file_name'];
				}

				if ($this->stadium->insertar($datosNuevoEstadio)) {
		      $this->session->set_flashdata("confirmacion","Agregado correctamente");
		    } else {
		      $this->session->set_flashdata("error","Error al guardar intente denuevo");
		    }
		    redirect('stadiums/index');
		  }

//funcion para Eliminar
	public function eliminar($id_stadium)
	{
    if ($this->stadium->borrar($id_stadium)) { //invocando al modelo
    redirect('stadiums/index');
  } else {
    echo "ERROR AL BORRAR :C";
  }
		redirect ('stadiums/index');

	}
//FUNCION REEDERIZAR VISTA EDITAR
public function editar($id_stadium){
	$data ["EstadioEditar"]=$this->stadium->obtenerPorId($id_stadium);
	$this->load->view('header')	;
	$this->load->view('stadiums/edit',$data);
	$this->load->view('footer')	;
}

//PROCESO DE ACTUALIZACION
public function procesarActualizacion(){
	$datosEditados=array(
    "name_stadium"=>$this->input->post('name_stadium'),
    "address_stadium"=>$this->input->post('address_stadium')
  );

  $this->load->library("upload");
      $new_name = "photo_stadium" . time() . "_" . rand(1, 5000);
      $config['file_name'] = $new_name . '_1';
      $config['upload_path']          = FCPATH . 'uploads/';
      $config['allowed_types']        = 'jpeg|jpg|png';
      $config['max_size']             = 1024*5; //5 MB
      $this->upload->initialize($config);

      if ($this->upload->do_upload("photo_stadium")) {
        $dataSubida = $this->upload->data();
        $datosEditados["photo_stadium"] = $dataSubida['file_name'];
      }
// $id=$this->input->post("id_stadium")
if ($this->stadium->actualizar($id,$datosEditados)) {

  redirect('stadiums/index');

}else {
  echo "<h1>ERRORE AL EDITAR </h1>";
}



}
} // cierre de la clase
