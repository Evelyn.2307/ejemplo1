<?php
class Customers extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    //cargamos el modelo
    $this->load->model('Customer');
  }
  //funcion para renderizar la vista
  public function index(){
    $data['customers']=$this->Customer->ObtenerTodos();
    $this->load->view('header');
    $this->load->view('customers/index',$data);
    $this->load->view('footer');
  }
  //funcion para renderizar la vista NUEVO
  public function new()
  {
    $this->load->view('header');
    $this->load->view('customers/new');
    $this->load->view('footer');
  }
  //funcion para guardar los datos
  public function save(){
    $datosNuevoCustomer=array(
      //convocamos al modelo
      "name_cus "=>$this->input->post('name_cus'),
      "last_name_cus"=>$this->input->post('last_name_cus'),
      "dni_cus"=>$this->input->post('dni_cus'),
      "cell_phone_cus"=>$this->input->post('cell_phone_cus'),
      "email_cus"=>$this->input->post('email_cus'),
      "address_cus"=>$this->input->post('address_cus'),
      "gender_cus"=>$this->input->post('gender_cus')


    );
    if ($this->Customer->insertar($datosNuevoCustomer)) {
      $this->session->set_flashdata("confirmation","Agregado correctamente");
    } else {
      $this->session->set_flashdata("error","Error al guardar intente denuevo");
    }
    redirect('customers/index');
  }
  //funcion para eliminar INGREDIENTES
  public function delete($id_cus){

    if($this->Customer->borrar($id_cus)) {
      $this->session->set_flashdata("confirmacion","Successfully deleted.");
    }else{
      $this->session->set_flashdata("error","Error al eliminar");
    }
    redirect('customers/index');
  }
  //funcion para rendrizar vista editar con  ingredientes
  public function edit($id_cus){
    $data["customerEditar"]=$this->Customer->obtenerPorId($id_cus);
    $this->load->view('header');
    $this->load->view('customers/edit',$data);
    $this->load->view('footer');
  }
  //proceso ACTUALIZAR
  public function procesarActualizacion(){
      $datosEditados=array(
        "name_cus "=>$this->input->post('name_cus'),
        "last_name_cus"=>$this->input->post('last_name_cus'),
        "dni_cus"=>$this->input->post('dni_cus'),
        "cell_phone_cus"=>$this->input->post('cell_phone_cus'),
        "email_cus"=>$this->input->post('email_cus'),
        "address_cus"=>$this->input->post('address_cus'),
        "gender_cus"=>$this->input->post('gender_cus')

      );
      $id_cus=$this->input->post("id_cus");
      if ($this->Customer->actualizar($id_cus,$datosEditados)) {
        $this->session->set_flashdata("confirmation","localidad actualizado exitosamente");
      } else {
        $this->session->flashdata("confirmation","Error al eliminar intente de nuevo");
      }
      redirect("customers/index");
    }
}//cierre de clase no borrar




 ?>
