<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipos extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    //cargamos el modelo
    $this->load->model('Equipo');
  }
  //funcion para renderizar la vista
  public function index(){
    $data['equipos']=$this->Equipo->ObtenerTodos();
    $this->load->view('header');
    $this->load->view('equipos/index',$data);
    $this->load->view('footer');
  }
  //funcion para renderizar la vista NUEVO
  public function nuevo()
  {
    $this->load->view('header');
    $this->load->view('equipos/nuevo');
    $this->load->view('footer');
  }
  //funcion para guardar los datos
  public function guardar(){
    $datosNuevoEquipo=array(
      //convocamos al modelo
      "nombre_eq"=>$this->input->post('nombre_eq'),
      "ciudad_eq"=>$this->input->post('ciudad_eq')
    );
    $this->load->library("upload");
        $new_name = "foto_eq" . time() . "_" . rand(1, 5000);
        $config['file_name'] = $new_name . '_1';
        $config['upload_path']          = FCPATH . 'uploads/';
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['max_size']             = 1024*5;
        $this->upload->initialize($config);

        if ($this->upload->do_upload("foto_eq")) {
          $dataSubida = $this->upload->data();
          $datosNuevoEquipo["foto_eq"] = $dataSubida['file_name'];
        }
    if ($this->Equipo->insertar($datosNuevoEquipo)) {
      $this->session->set_flashdata("confirmacion","Agregado correctamente");
    } else {
      $this->session->set_flashdata("error","Error al guardar intente denuevo");
    }
    redirect('equipos/index');
  }
  //funcion para Eliminar
  	public function eliminar($id_eq)
  	{
      if ($this->Equipo->borrar($id_eq)) { //invocando al modelo
      redirect('equipos/index');
    } else {
      echo "ERROR AL BORRAR :C";
    }
  		redirect ('equipos/index');

  	}

  //funcion para rendrizar vista editar
  public function editar($id_eq){
    $data["equipoEditar"]=$this->Equipo->obtenerPorId($id_eq);
    $data["EquipoEditar"]=$this->Equipo->obtenerPorId($id_eq);
    $this->load->view('header');
    $this->load->view('equipos/editar',$data);
    $this->load->view('footer');
  }
  //proceso ACTUALIZAR
  public function procesarActualizacion(){
      $datosEditados=array(
        "nombre_eq"=>$this->input->post('nombre_eq'),
        "ciudad_eq"=>$this->input->post('ciudad_eq')
      );
      $id_eq=$this->input->post("id_eq");
      if ($this->Equipo->actualizar($id_eq,$datosEditados)) {
        $this->session->set_flashdata("confirmacion","equipo actualizado exitosamente");
      } else {
        $this->session->flashdata("confirmacion","Error al eliminar intente de nuevo");
      }
      redirect("equipos/index");
    }
}//cierre de clase no borrar




 ?>
