<?php
class Partidos extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    //cargamos el modelo
    $this->load->model('Partido');
    $this->load->model ('Equipo');
  }
  //funcion para renderizar la vista
  public function index(){
    $data['partidos']=$this->Partido->ObtenerTodos();
    $this->load->view('header');
    $this->load->view('partidos/index',$data);
    $this->load->view('footer');
  }
  //funcion para renderizar la vista NUEVO
  public function nuevo()
  {
    $data['selectEquipo'] = $this->Equipo->obtenerTodos();

    $this->load->view('header');
    $this->load->view('partidos/nuevo',$data);
    $this->load->view('footer');
  }
  //funcion para guardar los datos
  public function guardar(){
    $datosNuevoPartido=array(
      //convocamos al modelo

      "fecha_partido"=>$this->input->post('fecha_partido'),
      "hora_partido"=>$this->input->post('hora_partido'),
      "equipo1_partido"=>$this->input->post('equipo1_partido'),
      "equipo2_partido"=>$this->input->post('equipo2_partido')
    );
    if ($this->Partido->insertar($datosNuevoPartido)) {
      $this->session->set_flashdata("confirmacion","Agregado correctamente");
    } else {
      $this->session->set_flashdata("error","Error al guardar intente denuevo");
    }
    redirect('partidos/index');
  }
  //funcion para eliminar eventos
  public function eliminar($id_partido){

    if($this->Partido->borrar($id_partido)) {
      $this->session->set_flashdata("confirmacion","Eliminado exitosamente");
    }else{
      $this->session->set_flashdata("error","Error al eliminar");
    }
    redirect('partidos/index');
  }
  //funcion para rendrizar vista editar con  eventos
  public function editar($id_partido){
    $data["partidoEditar"]=$this->Partido->obtenerPorId($id_partido);
    $this->load->view('header');
    $this->load->view('partidos/editar',$data);
    $this->load->view('footer');
  }
  //proceso ACTUALIZAR
  public function procesarActualizacion(){
      $datosEditados=array(
        "fecha_partido"=>$this->input->post('fecha_partido'),
        "hora_partido"=>$this->input->post('hora_partido'),
        "equipo1_partido"=>$this->input->post('equipo1_partido'),
        "equipo2_partido"=>$this->input->post('equipo2_partido')
      );
      $id_partido=$this->input->post("id_partido");
      if ($this->Partido->actualizar($id_partido,$datosEditados)) {
        $this->session->set_flashdata("confirmacion","evento actualizado exitosamente");
      } else {
        $this->session->flashdata("confirmacion","Error al eliminar intente de nuevo");
      }
      redirect("partidos/index");
    }
}//cierre de clase no borrar


 ?>
