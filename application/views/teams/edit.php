<br>
<h1 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px"><i class="glyphicon glyphicon"></i>UPGRADE TEAMS</h1>
<br><form class=""
id="frm_edit_team"
action="<?php echo site_url(); ?>/teams/create"
method="post" enctype="multipart/form-data">
<!-- ENCTYPE="MULTIPART/FORM-DATA"  PARA QUE LOS ARCHIVOS PUEDA VIAJAR -->
<!-- //en action llamamos al site_url al controlador INSTRUCTORES y funcion GURADAR -->
    <div class="row">
      <div class="col-md-1">

      </div>
      <div class="clo-md-3">
        <label for="">Photo:
            <span class="mandatory">*</span>
        </label>

        <input type="file" name="photo_te" placeholder="Enter the Photo"
        id="photo_te" value="<?php echo $TeamEdit->photo_te; ?>">
      </div>
        <input type="hidden" name="id_te" id="id_te" value="<?php echo $TeamEdit->id_te; ?>">
      <div class="col-md-3" >
          <label for="">Name:
            <span class="mandatory">*</span>
          </label>
          <br>
          <input type="text"
          placeholder="Enter the team name"
          class="form-control"
          name="name_te" value="<?php echo $TeamEdit->name_te; ?>"
          id="name_te">
      </div>
      <div class="col-md-3">
          <label for="">City:
            <span class="mandatory">*</span>

          </label>
          <br>
          <input type="text"
          placeholder="Enter city"
          class="form-control" required
          name="city_te" value="<?php echo $TeamEdit->city_te; ?>"
          id="city_te">
      </div>
    <div class="col-md-1">
    </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-warning">
            <i class="glyphicon glyphicon-edit"></i>
              Update
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/teams/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-remove-sign"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_edit_team").validate({
  rules:{
      name_te:{
        required:true,
        minlength:3,
        letras:true
      },
      city_te:{
        required:true,
        minlength:0,

      },
      photo_te:{
        required:true,
      }
  },
  messages:{
    name_te:{
      required:"This field is required",
      minlength:"enter a valid name",
      letras: "This field only accepts letters"
    },
    city_te:{
      required:"This field is required",
      minlength:"Please enter a valid city",

    },
    photo_te:{
      required:"This field is required",
    },
  }
});
</script>
<script type="text/javascript">
$("#photo_te").fileinput({language:'es'});

</script>
