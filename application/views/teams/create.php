<br>
<h1 class="text-center" style="color:white; background-color:#01ADFF ; padding: 10px 20px"> <i class="glyphicon glyphicon-plus"></i> NEW TEAMS</h1>
<form class=""
id="frm_create_team"
action="<?php echo site_url(); ?>/teams/save"
method="post" enctype="multipart/form-data">

    <div class="row">
      <div class="col-md-1">

      </div>
      <div class="col-md-3">
        <label for="">Photo:
            <span class="mandatory">*</span>
        </label>

        <input type="file" name="photo_te" placeholder="Enter the photo"
        id="photo_te" value="" required>
      </div>
      <div class="col-md-3">
          <label for="">Name:
<span class="mandatory">*</span>
          </label>
          <br>
          <input type="text"
          placeholder="Enter the team name"
          class="form-control"
          name="name_te" value=""
          id="name_te">
      </div>
      <div class="col-md-4">
          <label for="">City:
            <span class="mandatory">*</span>

          </label>
          <br>
          <input type="text"
          placeholder="Enter city"
          class="form-control" required
          name="city_te" value=""
          id="city_te">
      </div>


    </div>
    <br>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="glyphicon glyphicon-new"></i>
              Save
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/teams/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-remove-sign"></i>
              Cancel
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_create_team").validate({
  rules:{
      name_te:{
        required:true,
        minlength:3,
        letras:true
      },
      city_te:{
        required:true,
        minlength:3,

      },

      photo_te:{
        required:true,
      },
  },
  messages:{
    name_te:{
      required:"This field is required",
      minlength:"enter a valid name",
      letras: "This field only accepts letters"
    },
    city_te:{
      required:"This field is required",
      minlength:"Please enter a valid city",

    },
    photo_te:{
      required:"This field is required",
    },
  }
});
</script>
<script type="text/javascript">
$("#photo_te").fileinput({language:'es'});

</script>
