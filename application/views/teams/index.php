<br>
<h1 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px"><i class="glyphicon glyphicon"></i> TEAMS</h1>
<br>

<div class="row">
  &nbsp;&nbsp;
  <a href="<?php echo site_url(); ?>/teams/create" class="btn btn-success">
<i class="glyphicon glyphicon-plus"></i>
Add Teams
  </a>
</div>
<br>
<?php if ($teams): ?>
    <table class="table table-striped table-bordered table-hover" style="background-color:#005F8B ; color:white"id="tbl_teams">
        <thead>
           <tr>
             <th>ID</th>
             <th>PHOTO</th>
             <th>NAME</th>
             <th>CITY</th>
             <th>ACCIONES</th>
           </tr>
         </thead>
         <tbody style="background-color:#01ADFF;color:black">
           <?php foreach ($teams as $filaTemporal): ?>
             <tr>
               <td><?php echo $filaTemporal->id_te;?></td>
               <td>
               <?php if($filaTemporal->photo_te!=""):?>
                 <img src="<?php echo base_url('uploads/').$filaTemporal->photo_te; ?>" alt="" width="70px" height="70px" style="border-radius:50%">
               <?php else: ?>
                 N/A
               <?php endif; ?>
             </td>
               <td><?php echo $filaTemporal->name_te; ?></td>
               <td><?php echo $filaTemporal->city_te; ?></td>
               <td class="text-center">
               <a href="<?php echo site_url(); ?>/teams/edit/<?php echo $filaTemporal->id_te;?>" title="Edit">
                 <button type="submit" name="button" class="btn btn-warning">
                   <i class="glyphicon glyphicon-edit" style="color:white"></i>
                   Edit
                 </button>

                 </a>
                 &nbsp; &nbsp; &nbsp;

                   <a href="<?php echo site_url(); ?>/teams/borrar/<?php echo $filaTemporal->id_te;?>" title="Delete">
                     <button type="submit" name="button" class="btn btn-danger">
                       <i class="glyphicon glyphicon-trash" style="color:white"></i>
                       Delete
                     </button>
                   </a>

               </td>

             </tr>
           <?php endforeach; ?>
         </tbody>
       <?php else: ?>
       <h1>There are no teams</h1>
       <?php endif; ?>
  </table>


<script type="text/javascript">
  $("#tbl_teams").dataTable();
</script>
