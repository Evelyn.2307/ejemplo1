<br>
<br>
<center><h1><font color="blue">LOCATIONS</font></h1></center>
<br>
<br>
<form class=""
id="frm_new_locations"
action="<?php echo site_url(); ?>/locations/save"
method="post">
    <div class="row">
      <div class="col-md-4">
        <label for="">Gate:
          <span class="obligatorio">(Required)</span>
        </label>
        <br>
        <input type="number"
        placeholder="Enter the gate number"
        class="form-control"
        required
        name="gate_loc" value=""
        id="gate_loc">
      </div>
      <div class="col-md-4">
        <label for="">Row:
          <span class="obligatorio">(Required)</span>
        </label>
        <br>
        <input type="number"
        placeholder="Enter the row number"
        class="form-control"
        required
        name="row_loc" value=""
        id="row_loc">
      </div>
      <div class="col-md-4">
        <label for="">Seat:
          <span class="obligatorio">(Required)</span>
        </label>
        <br>
        <input type="number"
        placeholder="Enter the seat number"
        class="form-control"
        required
        name="seat_loc" value=""
        id="seat_loc">
      </div>
      <div class="col-md-12">
        <br>
        <label for="">Price:
          <span class="obligatorio">(Required)</span>
        </label>
        <br>
        <input type="text"
        placeholder="Enter the ticket price"
        class="form-control"
        name="price_loc" value=""
        id="price_loc">
    </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary glyphicon glyphicon-floppy-save">
              Save
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/locations/index"
              class="btn btn-danger glyphicon glyphicon-remove">
              Cancel
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_new_locations").validate({
 rules:{
   gate_loc:{
     required:true,
     minlength:1,
     maxlength:1000,
     digits:true
   },
   row_loc:{
     required:true,
     minlength:1,
     maxlength:1000,
     digits:true
   },
   seat_loc:{
     required:true,
     minlength:1,
     maxlength:5000,
     digits:true
   },
   price_loc:{
     required:true,
     minlength:1,
     maxlength:5000,

       }
 },
 messages:{
   gate_loc:{
     required:"Please enter the gate number",
     minlength:"Incorrect gate, enter 1 digit",
     maxlength:"Incorrect gate, enter 4 digits.",
     digits:"This field only accepts numbers",
     number:"This field only accepts numbers"
   },
     row_loc:{
       required:"Please enter the row number",
       minlength:"Incorrect row, enter 1 digit.",
       maxlength:"Incorrect row, enter 4 digit.",
       digits:"This field only accepts numbers",
       number:"This field only accepts numbers"
   },
   seat_loc:{
     required:"Please enter the seat number.",
     minlength:"Incorrect seat, enter 1 digit",
     maxlength:"Incorrect seat, enter 4 digit",
     digits:"This field only accepts numbers",
     number:"This field only accepts numbers"
   },
   price_loc:{
     required:"Please enter the ticket price",
     minlength:"Incorrect row, enter 1 digit.",
     maxlength:"Incorrect row, enter 4 digit.",
     }

     }
});

</script>
