<br>
<legend class="text-center">
  <br>
  <br>
  <i class="glyphicon glyphicon-pencil"></i>
  <b><font color="blue">EDIT LOCATION</font></b>
</legend>
<br>
<form class=""
id="frm_nuevo_location"
 action="<?php echo site_url("locations/procesarActualizacion"); ?>" method="post">
 <div class="row">
   <input type="text" class="hidden"name="id_loc" id="id_loc" value="<?php echo $locationEditar->id_loc; ?>">
   <div class="col-md-4">
       <label for="">Gate:
         <span class="obligatorio">(required)</span>
       </label>
       <br>
       <input type="number"
       placeholder="Enter the gate number"
       class="form-control"
       required
       name="gate_loc" value="<?php echo $locationEditar->gate_loc; ?>"
       id="gate_loc">
   </div>
   <div class="col-md-4">
       <label for="">Row:
         <span class="obligatorio">(required)</span>
       </label>
       <br>
       <input type="number"
       placeholder="Enter the row number"
       class="form-control"
       required
       name="row_loc" value="<?php echo $locationEditar->row_loc; ?>"
       id="row_loc">
   </div>
   <div class="col-md-4">
       <label for="">Seat:
         <span class="obligatorio">(required)</span>
       </label>
       <br>
       <input type="number"
       placeholder="Enter the seat number"
       class="form-control"
       required
       name="seat_loc" value="<?php echo $locationEditar->seat_loc; ?>"
       id="seat_loc">
   </div>

   <div class="col-md-4">
     <br>
     <label for="">Price:
         <span class="obligarotio">(required)</span>
     </label>
     <br>
     <input type="text"
     placeholder="Enter the ticket price"
     class="form-control"
     name="price_loc" value="<?php echo $locationEditar->price_loc; ?>"
     id="price_loc">
   </div>
 <br>
 </div>
 <br>
 <div class="row">
     <div class="col-md-12 text-center">
         <button type="submit" name="button"
         class="btn btn-primary">
          Update
         </button>
         &nbsp;
         <a href="<?php echo site_url(); ?>/locations/index"
           class="btn btn-danger">
           Cancel
         </a>
     </div>
 </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_location").validate({
 rules:{
       gate_loc:{
         required:true,
         minlength:1,
         maxlength:1000,
         digits:true
       },
       row_loc:{
         required:true,
         minlength:1,
         maxlength:1000,
         digits:true
       },
       seat_loc:{
         required:true,
         minlength:1,
         maxlength:5000,
         digits:true
       },
       price_loc:{
         required:true,
         minlength:1,
         maxlength:5000

       }
 },
 messages:{
     gate_loc:{
       required:"Please enter the gate number",
       minlength:"Incorrect gate, enter 1 digit",
       maxlength:"Incorrect gate, enter 4 digits.",
       digits:"This field only accepts numbers",
       number:"This field only accepts numbers"
     },
       row_loc:{
         required:"Please enter the row number",
         minlength:"Incorrect row, enter 1 digit.",
         maxlength:"Incorrect row, enter 4 digit.",
         digits:"This field only accepts numbers",
         number:"This field only accepts numbers"
     },
     seat_loc:{
       required:"Please enter the seat number.",
       minlength:"Incorrect seat, enter 1 digit",
       maxlength:"Incorrect seat, enter 4 digit",
       digits:"This field only accepts numbers",
       number:"This field only accepts numbers"
     },
     price_loc:{
       required:"Please enter the ticket price",
       minlength:"Incorrect row, enter 1 digit",
       maxlength:"Incorrect row, enter 4 digit"

     }

     }
});

</script>
