<br>
<br>
<div class="row">
  <div class="col-md-8">
    <h1 class=" glyphicon glyphicon-list-alt" style="color:blue"> LIST OF LOCATIONS</h1>
  </div>
  <br>
  <div class="col-md-4">
    <a href="<?php echo site_url("locations/new") ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
       Add location
    </a>
  </div>
</div>
<br>
<br>
<?php if ($locations): ?>
<table class="table table-striped table-bordered table-hover" id="tbl_locations">
  <thead>
    <tr>
      <th>ID</th>
      <th>GATE</th>
      <th>ROW</th>
      <th>SEAT</th>
      <th>PRICE</th>
      <th>ACTIONS</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($locations
             as $filaTemporal): ?>
               <tr>
                   <td><?php echo$filaTemporal->id_loc; ?></td>
                   <td><?php echo$filaTemporal->gate_loc; ?></td>
                   <td><?php echo$filaTemporal->row_loc; ?></td>
                   <td><?php echo$filaTemporal->seat_loc; ?></td>
                    <td><?php echo$filaTemporal->price_loc; ?></td>
              <td class="text-center">
            <a href="<?php echo site_url(); ?>/locations/edit/<?php echo $filaTemporal->id_loc; ?>" title="Edit locations" ;>
              <button type="submit" name="button" class="btn btn-warning">
              <i class="glyphicon glyphicon-edit"></i>
                   Edit
            </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url(); ?>/locations/delete/<?php echo $filaTemporal->id_loc; ?>" title="Delete Locations"
            onclick="return confirm('¿Are you sure to Delete permanently ?');"
            style="color:red;">
              <button type="submit" name="button" class="btn btn-danger">
              <i class="glyphicon glyphicon-trash"></i>
              Delete
            </button>
            </a>

          </td>
               </tr>
             <?php endforeach; ?>
  </tbody>
</table>
<?php else: ?>
  <h1>No information :(</h1>
  <center><h1><font color="red">No information added :(</font></h1></center>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl_locations").
  dataTable();
</script>
