<br>
<h1 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px"><i class="glyphicon glyphicon"></i> UPDATE STADIUMS</h1>
<br><form class=""
id="frm_editar_estadio"
action="<?php echo site_url(); ?>/stadiums/guardar"
method="post" enctype="multipart/form-data">
<!-- ENCTYPE="MULTIPART/FORM-DATA"  PARA QUE LOS ARCHIVOS PUEDA VIAJAR -->
<!-- //en action llamamos al site_url al controlador INSTRUCTORES y funcion GURADAR -->
    <div class="row">
      <div class="col-md-1">

      </div>
      <div class="clo-md-3">
        <label for="">Photo:
            <span class="obligatorio">*</span>
        </label>

        <input type="file" name="foto_es" placeholder="Ingrese el archivo"
        id="foto_es" value="<?php echo $EstadioEditar->photo_stadium; ?>" required>
      </div>
        <input type="hidden" name="id_es" id="id_es" value="<?php echo $EstadioEditar->id_stadium; ?>">
      <div class="col-md-3" >
          <label for="">Name:
            <span class="obligatorio">*</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del estadio"
          class="form-control"
          name="nombre_es" value="<?php echo $EstadioEditar->name_stadium; ?>"
          id="nombre_es">
      </div>
      <div class="col-md-3">
          <label for="">Address:
            <span class="obligatorio">*</span>

          </label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion del estadio"
          class="form-control" required
          name="direccion_es" value="<?php echo $EstadioEditar->address_stadium; ?>"
          id="direccion_es">
      </div>



    <div class="col-md-1">

    </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-warning">
            <i class="glyphicon glyphicon-edit"></i>
              Update
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/stadiums/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-remove-sign"></i>
              Cancel
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_editar_estadio").validate({
  rules:{
      name_stadium:{
        required:true,
        minlength:3,
        letras:true
      },
      address_stadium:{
        required:true,
        minlength:0,

      },
      photo_stadium:{
        required:true,
      }
  },
  messages:{
    name_stadium:{
      required:"This field is required",
      minlength:"Enter a valid name",
      letras: "This field only accepts letters"
    },
    address_stadium:{
      required:"This field is required",
      minlength:"Enter a valid address",

    },
    photo_stadium:{
      required:"This field is required",
    },
  }
});
</script>
<script type="text/javascript">
$("#photo_stadium").fileinput({language:'en'});

</script>
