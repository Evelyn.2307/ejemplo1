<br>
<h1 class="text-center" style="color:white; background-color:#01ADFF ; padding: 10px 20px"> <i class="glyphicon glyphicon-plus"></i> NEW STADIUM</h1>
<form class=""
id="frm_nuevo_estadio"
action="<?php echo site_url(); ?>/stadiums/guardar"
method="post" enctype="multipart/form-data">
<!-- ENCTYPE="MULTIPART/FORM-DATA"  PARA QUE LOS ARCHIVOS PUEDA VIAJAR -->
<!-- //en action llamamos al site_url al controlador INSTRUCTORES y funcion GURADAR -->
    <div class="row">
      <div class="col-md-1">

      </div>
      <div class="clo-md-3">
        <label for="">Photo:
            <span class="obligatorio">*</span>
        </label>

        <input type="file" name="photo_stadium" placeholder="Ingrese el archivo"
        id="photo_stadium" value="" required>
      </div>
      <div class="col-md-3">
          <label for="">Name:
<span class="obligatorio">*</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del estadio"
          class="form-control"
          name="name_stadium" value=""
          id="name_stadium">
      </div>
      <div class="col-md-4">
          <label for="">Address:
            <span class="obligatorio">*</span>

          </label>
          <br>
          <input type="text"
          placeholder="Ingrese la dirección del estadio"
          class="form-control" required
          name="address_stadium" value=""
          id="address_stadium">
      </div>


    </div>
    <br>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="glyphicon glyphicon-save"></i>
              Save
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/stadiums/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-remove-sign"></i>
              Cancel
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_estadio").validate({
  rules:{
      name_stadium:{
        required:true,
        minlength:3,
        letras:true
      },
      address_stadium:{
        required:true,
        minlength:3,

      },

      photo_stadium:{
        required:true,
      },
  },
  messages:{
    name_stadium:{
      required:"This field is required",
      minlength:"Enter a valid name",
      letras: "This field only accepts letters"
    },
    address_stadium:{
      required:"This field is required",
      minlength:"Enter a valid address",

    },
    photo_stadium:{
    required:"This field is required",
    },
  }
});
</script>
<script type="text/javascript">
$("#photo_stadium").fileinput({language:'en'});

</script>
