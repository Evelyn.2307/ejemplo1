<br>
<h1 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px"><i class="glyphicon glyphicon"></i> STADIUMS</h1>
<br>

<div class="row">
  &nbsp;&nbsp;
  <a href="<?php echo site_url(); ?>/stadiums/new" class="btn btn-success">
<i class="glyphicon glyphicon-plus"></i>
Add stadium
  </a>
</div>
<br>

<?php if ($stadiums): ?>
    <table class="table table-striped table-bordered table-hover" style="background-color:#005F8B ; color:white"id="tbl_estadios">
        <thead>
           <tr>
             <th>ID</th>
             <th>PHOTO</th>
             <th>NAME</th>
             <th>ADDRESS</th>
             <th>ACTIONS</th>
           </tr>
         </thead>
         <tbody style="background-color:#01ADFF;color:black">
           <?php foreach ($stadiums as $filaTemporal): ?>
             <tr>
               <td><?php echo $filaTemporal->id_stadium;?></td>
               <td>
               <?php if($filaTemporal->photo_stadium!=""):?>
                 <img src="<?php echo base_url('uploads/').$filaTemporal->photo_stadium; ?>" alt="" width="70px" height="70px" style="border-radius:50%">
               <?php else: ?>
                 N/A
               <?php endif; ?>
             </td>
               <td><?php echo $filaTemporal->name_stadium; ?></td>
               <td><?php echo $filaTemporal->address_stadium; ?></td>
               <td class="text-center">
               <a href="<?php echo site_url(); ?>/stadiums/editar/<?php echo $filaTemporal->id_stadium;?>" title="Editar">
                 <button type="submit" name="button" class="btn btn-warning">
                   <i class="glyphicon glyphicon-edit" style="color:white"></i>
                   Edit
                 </button>

                 </a>
                 &nbsp; &nbsp; &nbsp;

                   <a href="<?php echo site_url(); ?>/stadiums/eliminar/<?php echo $filaTemporal->id_stadium;?>" title="Eliminar">
                     <button type="submit" name="button" class="btn btn-danger">
                       <i class="glyphicon glyphicon-trash" style="color:white"></i>
                       Delete
                     </button>
                   </a>

               </td>

             </tr>
           <?php endforeach; ?>
         </tbody>
       <?php else: ?>
       <h1>No stadiums available</h1>
       <?php endif; ?>
  </table>


<script type="text/javascript">
  $("#tbl_estadios").dataTable();
</script>
