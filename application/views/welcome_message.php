<br>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="https://www.fef.ec/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-10-at-11.23.58-1024x569.jpeg" alt="Germany" height="80px" width="110%">
    </div>

    <div class="item">
      <img src="<?php echo base_url(); ?>/assets/images/fut.png" alt="French" height="60px" width="110%">
    </div>

    <div class="item">
      <img src="https://png.pngtree.com/png-vector/20221003/ourmid/pngtree-football-ticket-card-modern-design-png-image_6256246.png" alt="English" height="60px" width="110%">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
