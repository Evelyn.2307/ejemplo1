<br>
<br>
<center><h1><font color="blue">CUSTOMERS</font></h1></center>
<br>
<br>
<form class=""
id="frm_new_customers"
action="<?php echo site_url(); ?>/customers/save"
method="post">
<div class="row">
  <div class="col-md-4">
      <label for="">Name:
          <span class="obligatorio">(Required)</span>
      </label>
      <br>
      <input type="text" placeholder="Enter the name" class="form-control" required name="name_cus" value=""
      id="name_cus">
  </div>
  <div class="col-md-4">
      <label for="">Last Name:
          <span class="obligatorio">(Required)</span>
      </label>
      <br>
      <input type="text" placeholder="Enter the last name" class="form-control" required name="last_name_cus" value="" id="last_name_cus">
  </div>
  <div class="col-md-4">
      <label for="">Dni:
          <span class="obligatorio">(Required)</span>
      </label>
      <br>
      <input type="number" placeholder="Enter the dni" class="form-control" required name="dni_cus" value="" id="dni_cus">
  </div>
</div> <!-- Cierra el primer div con la clase "row" aquí -->

<div class="row">
  <div class="col-md-4">
      <br>
      <label for="">Cell Phone:
          <span class="obligatorio">(Required)</span>
      </label>
      <br>
      <input type="number" placeholder="Enter the cellphone please" class="form-control" name="cell_phone_cus" value="" id="cell_phone_cus">
  </div>
  <div class="col-md-4">
      <br>
      <label for="">Email:
          <span class="obligatorio">(Required)</span>
      </label>
      <br>
      <input type="text" placeholder="Enter the email" class="form-control" name="email_cus" value="" id="email_cus">
  </div>
  <div class="col-md-4">
      <br>
      <label for="">Address:
          <span class="obligatorio">(Required)</span>
      </label>
      <br>
      <input type="text" placeholder="Enter the address" class="form-control" name="address_cus" value="" id="address_cus">
  </div>
</div> <!-- Cierra el segundo div con la clase "row" aquí -->

<br>

<div class="row">
  <div class="col-md-12">
    <br>
    <label for="gender_cus">Gender:
      <span class="obligatorio">(Required)</span>
    </label>
    <br>
    <select class="form-control" name="gender_cus" id="gender_cus" required>
      <option value="">Select Gender</option>
      <option value="male">Male</option>
      <option value="female">Female</option>
      <option value="other">Other</option>
    </select>
  </div>
</div>

<br>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary glyphicon glyphicon-floppy-save">
              Save
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/customers/index"
              class="btn btn-danger glyphicon glyphicon-remove">
              Cancel
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_new_customers").validate({
 rules:{
   name_cus:{
     required:true,
     minlength:1,
     maxlength:1000,
   },
   last_name_cus:{
     required:true,
     minlength:1,
     maxlength:1000,
   },
   dni_cus:{
     required:true,
     minlength:10,
     maxlength:10,
     digits:true
   },
   cell_phone_cus:{
     required:true,
     minlength:10,
     maxlength:10,
     digits:true

   },
   email_cus{
     required:true,
     minlength:1,
     maxlength:1000,
   },
   address_cus{
     required:true,
     minlength:1,
     maxlength:1000,
   },
   gender_cus
   Required:true
 },
 messages:{
   name_cus:{
     required:"Please enter the name",
     minlength:"Incorrect name, enter 1 letras",
     maxlength:"Incorrect name, enter 4 letras.",
   },
     last_name_cus:{
       required:"Please enter the last name",
       minlength:"Incorrect name, enter 1 letras",
       maxlength:"Incorrect name, enter 4 letras.",
   },
   dni_cus:{
     required:"Please enter the dni.",
     minlength:"Incorrect seat, enter 10 digit",
     maxlength:"Incorrect seat, enter 10 digit",
     digits:"This field only accepts numbers",
     number:"This field only accepts numbers"
   },
   cell_phone_cus:{
     required:"Please enter the cell phone.",
     minlength:"Incorrect seat, enter 10 digit",
     maxlength:"Incorrect seat, enter 10 digit",
     digits:"This field only accepts numbers",
     number:"This field only accepts numbers"
   },
   email_cus:{
     required:"Please enter the email",
     minlength:"Incorrect email, enter 19 letras",
     maxlength:"Incorrect email, enter 50 letras.",

   },
   address_cus:{
     required:"Please enter the address",
     minlength:"Incorrect address, enter 5 letras",
     maxlength:"Incorrect address, enter 50 letras.",
   },
   gender_cus:{
      required:"Please select the gender",
   }

     }
});

</script>
