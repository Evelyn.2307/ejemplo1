<br>
<br>
<div class="row">
  <div class="col-md-8">
    <h1 class=" glyphicon glyphicon-list-alt" style="color:blue"> LIST OF CUSTOMERS</h1>
  </div>
  <br>
  <div class="col-md-4">
    <a href="<?php echo site_url("customers/new") ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
       Add customers
    </a>
  </div>
</div>
<br>
<br>
<?php if ($customers): ?>
<table class="table table-striped table-bordered table-hover" id="tbl_customers">
  <thead>
    <tr>
      <th>ID</th>
      <th>NAME</th>
      <th>LAST NAME</th>
      <th>DNI</th>
      <th>CELL PHONE</th>
      <th>EMAIL</th>
      <th>ADDRESS</th>
      <th>GENDER</th>
      <th>ACTIONS</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($customers
             as $filaTemporal): ?>
               <tr>
                   <td><?php echo$filaTemporal->id_cus; ?></td>
                   <td><?php echo$filaTemporal->name_cus; ?></td>
                   <td><?php echo$filaTemporal->last_name_cus; ?></td>
                   <td><?php echo$filaTemporal->dni_cus; ?></td>
                    <td><?php echo$filaTemporal->cell_phone_cus; ?></td>
                    <td><?php echo$filaTemporal->email_cus; ?></td>
                    <td><?php echo$filaTemporal->address_cus; ?></td>
                    <td><?php echo$filaTemporal->gender_cus; ?></td>
              <td class="text-center">
            <a href="<?php echo site_url(); ?>/customers/edit/<?php echo $filaTemporal->id_cus; ?>" title="Edit customers" ;>
              <button type="submit" name="button" class="btn btn-warning">
              <i class="glyphicon glyphicon-edit"></i>
                   Edit
            </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url(); ?>/customers/delete/<?php echo $filaTemporal->id_cus; ?>" title="Delete customers"
            onclick="return confirm('¿Are you sure to Delete permanently ?');"
            style="color:red;">
              <button type="submit" name="button" class="btn btn-danger">
              <i class="glyphicon glyphicon-trash"></i>
              Delete
            </button>
            </a>

          </td>
               </tr>
             <?php endforeach; ?>
  </tbody>
</table>
<?php else: ?>
  <h1>No information :(</h1>
  <center><h1><font color="red">No information added :(</font></h1></center>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl_customers").
  dataTable();
</script>
