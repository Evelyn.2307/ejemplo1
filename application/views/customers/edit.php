<br>
<legend class="text-center">
  <br>
  <br>
  <i class="glyphicon glyphicon-pencil"></i>
  <b><font color="blue">EDIT CUSTOMERS</font></b>
</legend>
<br>
<form class=""
id="frm_nuevo_customer"
 action="<?php echo site_url("customers/procesarActualizacion"); ?>" method="post">
 <div class="row">
   <input type="text" class="hidden"name="id_cus" id="id_cus" value="<?php echo $customerEditar->id_cus; ?>">
   <div class="col-md-4">
       <label for="">Name:
         <span class="obligatorio">(required)</span>
       </label>
       <br>
       <input type="text"
       placeholder="Enter the name"
       class="form-control"
       required
       name="name_cus" value="<?php echo $customerEditar->name_cus; ?>"
       id="name_cus">
   </div>
   <div class="col-md-4">
       <label for="">Last Name:
         <span class="obligatorio">(required)</span>
       </label>
       <br>
       <input type="text"
       placeholder="Enter the last name"
       class="form-control"
       required
       name="last_name_cus" value="<?php echo $customerEditar->last_name_cus; ?>"
       id="last_name_cus">
   </div>
   <div class="col-md-4">
       <label for="">Dni:
         <span class="obligatorio">(required)</span>
       </label>
       <br>
       <input type="number"
       placeholder="Enter the Dni"
       class="form-control"
       required
       name="dni_cus" value="<?php echo $customerEditar->dni_cus; ?>"
       id="dni_cus">
   </div>
</div>
  <div class="row">
   <div class="col-md-4">
     <br>
     <label for="">Cell Phone:
         <span class="obligarotio">(required)</span>
     </label>
     <br>
     <input type="number"
     placeholder="Enter the cell phone"
     class="form-control"
     name="cell_phone_cus" value="<?php echo $customerEditar->cell_phone_cus; ?>"
     id="cell_phone_cus">
   </div>
   <div class="col-md-4">
     <br>
     <label for="">Email:
         <span class="obligarotio">(required)</span>
     </label>
     <br>
     <input type="text"
     placeholder="Enter the cell phone"
     class="form-control"
     name="email_cus" value="<?php echo $customerEditar->email_cus; ?>"
     id="email_cus">
   </div>
   <div class="col-md-4">
     <br>
     <label for="">Address:
         <span class="obligarotio">(required)</span>
     </label>
     <br>
     <input type="text"
     placeholder="Enter the cell phone"
     class="form-control"
     name="address_cus" value="<?php echo $customerEditar->address_cus; ?>"
     id="address_cus">
   </div>
 </div>
 <div class="row">
   <div class="col-md-12">
     <br>
     <label for="gender_cus">Gender:
       <span class="obligatorio">(Required)</span>
     </label>
     <br>
     <select class="form-control" name="gender_cus" value="<?php echo $customerEditar->gender_cus; ?>" id="gender_cus" >
       <option value="">Select Gender</option>
       <option value="male">Male</option>
       <option value="female">Female</option>
       <option value="other">Other</option>
     </select>
   </div>
 </div>
 <br>
 <div class="row">
     <div class="col-md-12 text-center">
         <button type="submit" name="button"
         class="btn btn-primary">
          Update
         </button>
         &nbsp;
         <a href="<?php echo site_url(); ?>/locations/index"
           class="btn btn-danger">
           Cancel
         </a>
     </div>
 </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_location").validate({
 rules:{
       gate_loc:{
         required:true,
         minlength:1,
         maxlength:1000,
         digits:true
       },
       row_loc:{
         required:true,
         minlength:1,
         maxlength:1000,
         digits:true
       },
       seat_loc:{
         required:true,
         minlength:1,
         maxlength:5000,
         digits:true
       },
       price_loc:{
         required:true,
         minlength:1,
         maxlength:5000

       }
 },
 messages:{
     gate_loc:{
       required:"Please enter the gate number",
       minlength:"Incorrect gate, enter 1 digit",
       maxlength:"Incorrect gate, enter 4 digits.",
       digits:"This field only accepts numbers",
       number:"This field only accepts numbers"
     },
       row_loc:{
         required:"Please enter the row number",
         minlength:"Incorrect row, enter 1 digit.",
         maxlength:"Incorrect row, enter 4 digit.",
         digits:"This field only accepts numbers",
         number:"This field only accepts numbers"
     },
     seat_loc:{
       required:"Please enter the seat number.",
       minlength:"Incorrect seat, enter 1 digit",
       maxlength:"Incorrect seat, enter 4 digit",
       digits:"This field only accepts numbers",
       number:"This field only accepts numbers"
     },
     price_loc:{
       required:"Please enter the ticket price",
       minlength:"Incorrect row, enter 1 digit",
       maxlength:"Incorrect row, enter 4 digit"

     }

     }
});

</script>
