<br>
<legend class="text-center">
  <br>
  <br>
  <i class="glyphicon glyphicon-pencil"></i>
  <b><font color="blue">EDITAR PARTIDOS</font></b>
</legend>
<br>
<form class=""
id="frm_nuevo_partido"
 action="<?php echo site_url("partidos/procesarActualizacion"); ?>" method="post">
 <div class="row">
   <input type="text" class="hidden"name="id_partido" id="id_partido" value="<?php echo $partidoEditar->id_partido; ?>">
   <div class="col-md-12">
     <label for="">Equipo Local:
       <span class="obligatorio">(Obligatorio)</span>
     </label>
     <br>
     <input type="text"
     placeholder="Ingrese el equipo local"
     class="form-control"
     required
     name="equipo1_partido" value="<?php echo $partidoEditar->equipo1_partido; ?>"
     id="equipo1_partido">
   </div>
   <div class="col-md-12">
     <label for="">Equipo Visitante:
       <span class="obligatorio">(Obligatorio)</span>
     </label>
     <br>
     <input type="text"
     placeholder="Ingrese el equipo visitante"
     class="form-control"
     required
     name="equipo2_partido" value="<?php echo $partidoEditar->equipo2_partido; ?>"
     id="equipo2_partido">
   </div>
   <div class="col-md-12">
     <label for="">Fecha:
       <span class="obligatorio">(Obligatorio)</span>
     </label>
     <br>
     <input type="date"
     placeholder="Ingrese la fecha"
     class="form-control"
     required
     name="fecha_partido" value="<?php echo $partidoEditar->fecha_partido; ?>"
     id="fecha_partido">
   </div>
   <div class="col-md-12">
     <label for="">Hora:
       <span class="obligatorio">(Obligatorio)</span>
     </label>
     <br>
     <input type="time"
     placeholder="Ingrese la hora"
     class="form-control"
     required
     name="hora_partido" value="<?php echo $partidoEditar->hora_partido; ?>"
     id="hora_partido">
   </div>
 </div>
 <br>
 <br>
 <div class="row">
     <div class="col-md-12 text-center">
         <button type="submit" name="button"
         class="btn btn-primary glyphicon glyphicon-floppy-save">
           Guardar
         </button>
         &nbsp;
         <a href="<?php echo site_url(); ?>/partidos/index"
           class="btn btn-danger glyphicon glyphicon-remove">
           Cancelar
         </a>
     </div>
 </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_partido").validate({
rules:{
    equipo1_partido:{
      required:true
    },
    equipo2_partido:{
      required:true
    },
    fecha_partido:{
      required:true
    },
    hora_partido:{
      required:true
    }
},
messages:{
  equipo1_partido:{
    required:"Este campo es requerido"

  },
    equipo2_partido:{
      required:"Este campo es requerido"
  },
  fecha_partido:{
    required:"Este campo es requerido",

  },
  hora_partido:{
    required:"Este campo es requerido"
  }

  }
});

</script>
