<br>
<br>
<center><h1><font color="blACK">PARTIDOS</font></h1></center>
<br>
<br>
<?php if ($selectEquipo) {?>
<form class=""

id="frm_nuevo_partido"
action="<?php echo site_url(); ?>/partidos/guardar"
method="post">

    <div class="row">
      <div class="col-md-6">
        <label for="equipo1_partido">Equipo Local:
          <span class="obligatorio">(Obligatorio)</span>
        </label>
        <select class="form-select form-select" name="equipo1_partido" id="equipo1_partido">
                          <?php foreach($selectEquipo as $equipo1){ ?>
                          <option selected disabled>Seleccione una</option>
                          <option value="<?php echo $equipo1->id_eq ?>"><?php echo $equipo1->nombre_eq; ?></option>
                          <?php } ?>


                      </select>
<br><br>
                        </div>
      <div class="col-md-6">
        <label for="">Equipo Visitante:
          <span class="obligatorio">(Obligatorio)</span>
        </label>
        <select class="form-select form-select" name="equipo1_partido" id="equipo1_partido">
                          <?php foreach($selectEquipo as $equipo2){ ?>
                          <option selected disabled>Seleccione una</option>
                          <option value="<?php echo $equipo2->id_eq ?>;"><?php echo $equipo2->nombre_eq; ?></option>
                          <?php } ?>


                      </select>
                      <br>
      </div>
      <div class="col-md-12">
        <label for="">Fecha:
          <span class="obligatorio">(Obligatorio)</span>
        </label>
        <br>
        <input type="date"
        placeholder="Ingrese la fecha"
        class="form-control"
        required
        name="fecha_partido" value=""
        id="fecha_partido">
      </div>
      <div class="col-md-12">
        <label for="">Hora:
          <span class="obligatorio">(Obligatorio)</span>
        </label>
        <br>
        <input type="time"
        placeholder="Ingrese la hora"
        class="form-control"
        required
        name="hora_partido" value=""
        id="hora_partido">
      </div>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary glyphicon glyphicon-floppy-save">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/partidos/index"
              class="btn btn-danger glyphicon glyphicon-remove">
              Cancelar
            </a>
        </div>
    </div>
</form>
<?php }else{
            echo "No existen equipos";
        } ?>

<script type="text/javascript">
$("#frm_nuevo_partido").validate({
 rules:{
       equipo1_partido:{
         required:true
       },
       equipo2_partido:{
         required:true
       },
       fecha_partido:{
         required:true
       },
       hora_partido:{
         required:true
       }
 },
 messages:{
     equipo1_partido:{
       required:"Este campo es requerido"

     },
       equipo2_partido:{
         required:"Este campo es requerido"
     },
     fecha_partido:{
       required:"Este campo es requerido",

     },
     hora_partido:{
       required:"Este campo es requerido"
     }

     }
});


</script>
