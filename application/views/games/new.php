<br>
<br>
<center><h1><font color="blACK">GAMES</font></h1></center>
<br>
<br>
<center>
<form class=""
id="frm_new_game"
action="<?php echo site_url(); ?>/games/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
        <label for="">DAY:
          <span class="mandatory">(Mandatory)</span>
        </label>
        <br>
        <input type="text"
        placeholder=" Enter the day"
        class="form-control"
        required
        name="day_eve" value=""
        id="day_eve">
      </div>
      <div class="col-md-4">
        <label for="">DATE:
          <span class="mandatory">(Mandatory)</span>
        </label>
        <br>
        <input type="date"
        placeholder="Enter the date"
        class="form-control"
        required
        name="date_eve" value=""
        id="date_eve">
      </div>
      <div class="col-md-4">
        <label for="">HOUR:
          <span class="mandatory">(Mandatory)</span>
        </label>
        <br>
        <input type="time"
        placeholder="Enter the hour"
        class="form-control"
        required
        name="hour_eve" value=""
        id="hour_eve">
      </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary glyphicon glyphicon-floppy-save">
               keep
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/games/index"
              class="btn btn-danger glyphicon glyphicon-remove">
              Cancel
            </a>
        </div>
    </div>
</form>
</center>

<script type="text/javascript">
$("#frm_new_game").validate({
 rules:{
       dia_eve:{
         required:true,
         minlength:1,
       },
       fecha_eve:{
         required:true,
       },
       hora_eve:{
         required:true
       }
 },
 messages:{
     day_eve:{
       required:"Please enter the day",
       minlength:"day incorrect, enter 1 digitos",
       maxlength:"day incorrecta, ingrese 4 digitos",

     },
       date_eve:{
         required:"Please enter the date",
     },
     hour_eve:{
       required:"Please enter the hour",
     }

     }
});

</script>
