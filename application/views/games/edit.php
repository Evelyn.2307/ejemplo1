<br>
<legend class="text-center">
  <br>
  <br>
  <i class="glyphicon glyphicon-pencil"></i>
  <b><font color="blue">EDIT GAMES</font></b>
</legend>
<br>
<form class=""
id="frm_new_event"
 action="<?php echo site_url("games/procesarActualizacion"); ?>" method="post">
 <div class="row">
   <input type="text" class="hidden"name="id_eve" id="id_eve" value="<?php echo $gameEdit->id_eve; ?>">
   <div class="col-md-12">
       <label for="">Day:
         <span class="mandatory">(Mandatory)</span>
       </label>
       <br>
       <input type="text"
       placeholder="Enter the day"
       class="form-control"
       required
       name="day_eve" value="<?php echo $gameEdit->day_eve; ?>"
       id="day_eve">
   </div>
   <div class="col-md-12">
       <label for="">Date:
         <span class="mandatory">(Mandatory)</span>
       </label>
       <br>
       <input type="date"
       placeholder="Enter the date"
       class="form-control"
       required
       name="date_eve" value="<?php echo $gameEdit->date_eve; ?>"
       id="date_eve">
   </div>
   <div class="col-md-12">
       <label for="">Hour:
         <span class="mandatory">(Mandatory)</span>
       </label>
       <br>
       <input type="time"
       placeholder="Enter the hour"
       class="form-control"
       required
       name="hour_eve" value="<?php echo $gameEdit->hour_eve; ?>"
       id="hour_eve">
   </div>
 <br>
 </div>
 <br>
 <div class="row">
     <div class="col-md-12 text-center">
         <button type="submit" name="button"
         class="btn btn-primary">
           Update
         </button>
         &nbsp;
         <a href="<?php echo site_url(); ?>/games/index"
           class="btn btn-danger">
           Cancel
         </a>
     </div>
 </div>
</form>

<script type="text/javascript">
$("#frm_new_game").validate({
 rules:{
       day_eve:{
         required:true,
         minlength:1,
         maxlength:1000,
       },
       date_eve:{
         required:true,

       },
       hour_eve:{
         required:true,

       },
 },
 messages:{
     day_eve:{
       required:"Plese enter the day",
       minlength:"Day incorrect, ingrese 1 digitos",
       maxlength:"Day incorrect, ingrese 25 digitos",
     },
       date_eve:{
         required:"Please enter the date",

     },
     hour_eve:{
       required:"Please enter the hour",

     }
     }
});

</script>
