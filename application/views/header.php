partidos
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>SOOCER CHAMPIONSHIP </title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- site icon -->
      <link rel="icon" href="<?php echo base_url("plantilla/images/fevicon.png"); ?>" type="image/png" />
      <!-- bootstrap css -->
      <link rel="stylesheet" href="<?php echo base_url("plantilla/css/bootstrap.min.css"); ?>" />
      <!-- site css -->
      <link rel="stylesheet" href="<?php echo base_url("plantilla/style.css"); ?>" />
      <!-- responsive css -->
      <link rel="stylesheet" href="<?php echo base_url("plantilla/css/responsive.css"); ?>" />
      <!-- color css -->
      <link rel="stylesheet" href="<?php echo base_url("plantilla/css/colors.css"); ?>" />
      <!-- select bootstrap -->
      <link rel="stylesheet" href="<?php echo base_url("plantilla/css/bootstrap-select.css"); ?>" />
      <!-- scrollbar css -->
      <link rel="stylesheet" href="<?php echo base_url("plantilla/css/perfect-scrollbar.css") ;?>" />
      <!-- custom css -->
      <link rel="stylesheet" href="<?php echo base_url("plantilla/css/custom.css"); ?>" />
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!-- importacion del jquery -->
    <script
        src="https://code.jquery.com/jquery-3.7.0.js"integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>

    <!-- importacion de jquery validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- validacion de los nombres que solo pemritan letras y no numeros -->
    <script type="text/javascript">
      jQuery.validator.addMethod("letras", function(value, element) {
        //return this.optional(element) || /^[a-z]+$/i.test(value);
        return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñó ]*$/.test(value);

      }, "");
    </script>
    <!-- importacion de toastr -->
    <script  src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css"
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.3.6/css/buttons.dataTables.min.css">
  <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"> </script>
   </head>
   <body class="dashboard dashboard_1">
      <div class="full_container">
         <div class="inner_container">
            <!-- Sidebar  -->
            <nav id="sidebar">
               <div class="sidebar_blog_1">

                  <div class="sidebar_user_info">
                     <div class="icon_setting"></div>
                     <div class="user_profle_side">
                        <div class="user_img"><a href="<?php echo site_url('welcome/index'); ?>"><img class="img-responsive" src="<?php echo base_url("/assets/images/ticket-soccer.jpg"); ?>" alt="#" /></div>
                        <div class="user_info">
                           <h6>SOCCER TICKET</h6>
                           <p><span class="online_animation"></span> Online</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="sidebar_blog_2">
                  <h4>Home</h4>
                  <ul class="list-unstyled components">
                    <!-- <i class="menu-arrow"></i> -->

                     <li class="active">
                        <a href="#dashboard" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-flag yellow_color"></i> <span>STADIUM</span></a>
                        <ul class="collapse list-unstyled" id="dashboard">
                           <li>
                              <a href="<?php echo site_url('stadiums/new'); ?>">
                              <h11 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px"><i class="glyphicon glyphicon-plus"></i> NEW</h11></a>
                           </li>
                           <li>
                             <br>
                              <a href="<?php echo site_url('stadiums/index'); ?>">
                              <h11 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px"><i class="glyphicon glyphicon-check"></i> LIST</h11></a>
                           </li>
                        </ul>
                     </li>
                     <li>
                        <a href="#element" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-group red_color" aria-hidden="true"></i> <span>TEAMS</span></a>
                        <ul class="collapse list-unstyled" id="element">
                           <li><a href="<?php echo site_url('teams/create') ?>">
                             <h11 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px"><i class="glyphicon glyphicon-plus"></i> NEW</h11></a></li>
                           <li>
                             <br>
                             <a href="<?php echo site_url('teams/index') ?>">
                             <h11 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px"><i class="glyphicon glyphicon-check"></i> LIST</h11></a></li>
                           <!-- <li><a href="icons.html">> <span>Icons</span></a></li>
                           <li><a href="invoice.html">> <span>Invoice</span></a></li> -->
                        </ul>
                     </li>
                     <li>
                        <a href="#apps" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-object-group blue2_color"></i> <span>GAMES</span></a>
                        <ul class="collapse list-unstyled" id="apps">
                           <li><a href="<?php echo site_url('partidos/nuevo') ?>">
                             <h11 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px"><i class="glyphicon glyphicon-plus"></i> NUEVO</h11></a>
                           </a></li>
                           <li>
                             <br>
                             <a href="<?php echo site_url('partidos/index') ?>">
                             <h11 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px"><i class="glyphicon glyphicon-check"></i> LISTADO</h11></a>
                        </ul>
                     </li>
                     <li class="active">
                        <a href="#additional_page" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-futbol-o" aria-hidden="true"> </i> <span>LOCATIONS</span></a>
                        <ul class="collapse list-unstyled" id="additional_page">
                           <li><a href="<?php echo site_url('locations/new') ?>">
                             <h11 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px"><i class="glyphicon glyphicon-plus"></i> NEW</h11></a>
                           </a>
                           </li>
                           <li>
                             <br>
                              <a href="<?php echo site_url('locations/index') ?>">
                                <h11 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px"><i class="glyphicon glyphicon-check"></i> LIST</h11></a></a>
                           </li>

                        </ul>
                     </li>
                     <li>
        <a href="#clientes" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
            <i class="fa fa-user green_color"></i> <span>CUSTOMERS</span>
        </a>
        <ul class="collapse list-unstyled" id="clientes">
            <li>
                <a href="<?php echo site_url('customers/new'); ?>">
                    <h11 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px">
                        <i class="glyphicon glyphicon-plus"></i> NEW
                    </h11>
                </a>
            </li>
            <li>
                <br>
                <a href="<?php echo site_url('customers/index'); ?>">
                    <h11 class="text-center" style="color:white; background-color:#01ADFF ; padding:10px 20px">
                        <i class="glyphicon glyphicon-check"></i> LIST
                    </h11>
                </a>
            </li>
        </ul>
    </li>

                   <!--<li><a href="map.html"><i class="fa fa-map purple_color2"></i> <span>Map</span></a></li>
                     <li><a href="charts.html"><i class="fa fa-bar-chart-o green_color"></i> <span>Charts</span></a></li>
                     <li><a href="settings.html"><i class="fa fa-cog yellow_color"></i> <span>Settings</span></a></li> -->
                  </ul>
               </div>
            </nav>
            <!-- end sidebar -->
            <!-- right content -->
            <div id="content">
               <!-- topbar -->
               <div class="topbar">
                  <nav class="navbar navbar-expand-lg navbar-light">
                     <div class="full">
                        <button type="button" id="sidebarCollapse" class="sidebar_toggle"><i class="fa fa-bars"></i></button>
                        <div class="logo_section">
                           <a href="#"><img class="img-responsive" src="<?php echo base_url("plantilla/images/logo/logo.png"); ?>" alt=""></a>
                        </div>
                        <div class="right_topbar">
                           <div class="icon_info">
                              <ul>
                                 <li><a href="#"><i class="fa fa-bell-o"></i></a></li>
                                 <li><a href="#"><i class="fa fa-question-circle"></i></a></li>
                                 <li><a href="#"><i class="fa fa-envelope-o"></i><span class="badge">3</span></a></li>
                              </ul>
                              <ul class="user_profile_dd">
                                 <li>
                                    <a class="dropdown-toggle" data-toggle="dropdown"> <img class="img-responsive rounded-circle" src="<?php echo base_url("plantilla/images/layout_img/user_img.jpg"); ?>"alt="#" /><span class="name_user">  <!-- aqui llamamos al correo conectado -->

                                    <div class="dropdown-menu">
                                       <!-- <a class="dropdown-item" href="profile.html">My Profile</a>
                                       <a class="dropdown-item" href="settings.html">Settings</a>
                                       <a class="dropdown-item" href="help.html">Help</a> -->
                                       <a class="dropdown-item" href="<?php echo site_url(); ?>/seguridades/login"> <i class="fa fa-sign-out"></i><span><h><b>Iniciar Sesión</b></h></span></a>
                                       <!-- <a class="dropdown-item" href=""onclick="cerrarSistema();"><span>Salir</span> <i class="fa fa-sign-out"></i></a>
                                       <script type="text/javascript">
                                       function cerrarSistema(){
                                         windw.location.href="/ -->
                                         <a href="<?php echo site_url('seguridades/cerrarSesion'); ?>" > <i class="fa fa-power-off"></i><b>Salir</b>
                                         </a>
                                       </script>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </nav>
               </div>

               <!-- end topbar -->
