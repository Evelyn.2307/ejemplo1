<?php if ($this->session->flashdata("confirmacion")): ?>
  <script type="text/javascript">
	toastr.success("<?php echo $this->session->flashdata("confirmacion"); ?>");
  </script>
  <?php $this->session->set_flashdata("confirmacion","") ?>
<?php endif; ?>

<?php if ($this->session->flashdata("bienvenida")): ?>
  <script type="text/javascript">
	toastr.info("<?php echo $this->session->flashdata("bienvenida")?>");
  </script>
  <?php $this->session->set_flashdata("bienvenida","") ?>
<?php endif; ?>

<?php if ($this->session->flashdata("error")): ?>
  <script type="text/javascript">
	toastr.error(<?php echo $this->session->flashdata("error"); ?>);
  </script>
  <?php $this->session->set_flashdata("error","") ?>
<?php endif; ?>

<style media="screen">
  .obligatorio{
    color:red;
    background-color:white;
    border-radius: 10px;
    font-size: 15px;
    padding-left:10px;
    padding-right: 10px;
  }
  .error{
    color:red;
    font-weigth:bold;
  }
  input.error{
    border: 2px solid red;
  }
</style>
<div class="container-fluid">
                     <div class="footer">
                        <p>Copyright © 2023 SOCCER TICKET.<br><br>
                           <b>Developed by:</b><br> <a >ANTE MIRIAN</a><br>
                                                   <a>ESPIN NATALIA</a><br>
                                                   <a>SANGOQUIZA EVELYN</a><br>
                                                   <a>QUIMBITA CARMEN</a><br>
                        </p>
                     </div>
                  </div>
               </div>
               <!-- end dashboard inner -->
            </div>
         </div>
      </div>
      <!-- jQuery -->
      <script src="<?php echo base_url("plantilla/js/jquery.min.js") ?>"></script>
      <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
      <script src="<?php echo base_url("plantilla/js/popper.min.js") ?>"></script>
      <script src="<?php echo base_url("plantilla/js/bootstrap.min.js") ?>"></script>
      <!-- wow animation -->
      <script src="<?php echo base_url("plantilla/js/animate.js") ?>"></script>
      <!-- select country -->
      <script src="<?php echo base_url("plantilla/js/bootstrap-select.js") ?>"></script>
      <!-- owl carousel -->
      <script src="<?php echo base_url("plantilla/js/owl.carousel.js") ?>"></script>
      <!-- chart js -->
      <script src="<?php echo base_url("plantilla/js/Chart.min.js") ?>"></script>
      <script src="<?php echo base_url("plantilla/js/Chart.bundle.min.js") ?>"></script>
      <script src="<?php echo base_url("plantilla/js/utils.js") ?>"></script>
      <script src="<?php base_url("plantilla/js/analyser.js") ?>"></script>
      <!-- nice scrollbar -->
      <script src="<?php echo base_url("plantilla/js/perfect-scrollbar.min.js") ?>"></script>
      <script>
         var ps = new PerfectScrollbar('#sidebar');
      </script>
      <!-- custom js -->
      <script src="<?php echo base_url("plantilla/js/custom.js") ?>"></script>
      <script src="<?php echo base_url("plantilla/js/chart_custom_style1.js") ?>"></script>
   </body>
</html>
